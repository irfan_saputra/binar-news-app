import axios from 'axios';
import React, { useState } from 'react';
import { View, Text, FlatList, StyleSheet, Image, TouchableOpacity, Button, TextInput } from 'react-native';
import Config from 'react-native-config';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();


class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      dataSource: [],
      val: ''
    }
  }

  async componentDidMount() {
    try {
      let response = await axios.get(Config.API_URL + '/top-headlines?country=id&apiKey=' + Config.API_KEY + '&page=1')

      this.setState({
        isLoading: false,
        dataSource: response.data.articles
      })
    } catch (error) {
      console.log(error)
    }
  }

  async fetchAxiosSearch(val) {
    try {
      let response = await axios.get(Config.API_URL + '/everything?q=' + val + '&page=1&apiKey=' + Config.API_KEY)
      this.setState({
        isLoading: false,
        dataSource: response.data.articles
      })
    } catch (error) {
      console.error(error);
    }

  }

  async fetchAxiosHeadline() {
    try {
      let response = await axios.get(Config.API_URL + '/top-headlines?country=id&apiKey=' + Config.API_KEY + '&page=1')
      this.setState({
        isLoading: false,
        dataSource: response.data.articles
      })
    } catch (error) {
      console.error(error);
    }

  }
  _renderItem = ({ item, index }) => {
    const { navigation } = this.props;

    return (
      <View style={styles.containerNews}>
        <TouchableOpacity onPress={() => navigation.navigate('Detail', { url: item.url })}>
          <View style={styles.containerHeader}>
                    <Image style={styles.image} source={{ uri: item.urlToImage }}/>
                    <Text style={styles.title}>{item.title}</Text>
                </View>
                <Text style={[styles.text, {fontWeight: 'bold'}]}>{item.author}</Text>
                <Text style={styles.text}>{item.description}</Text>
                <Text style={[styles.text, {fontWeight: 'bold'}]}>{item.publishedAt}</Text>
        </TouchableOpacity>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.dataSource}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  item: {
    fontSize: 14,
    //padding: 5,
    // borderBottomWidth:1,
    // borderBottomColor:'#eee',
    borderWidth: StyleSheet.hairlineWidth,
    margin: 8,
    borderRadius: 12,
    backgroundColor: 'white'
  },
  // title: {
  //   fontSize: 14,
  //   fontWeight: 'bold'
  // },
  image: {
    flex: 1,
    height: 150,
    alignSelf: 'stretch',
    margin:5
  }, containerHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 5,
  },
  containerNews: {
    padding: 10,
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 15,
    margin: 10
  },
  title: {
    fontSize: 16,
    flex: 1,
    color: 'blue',
    marginRight: 5
  },
  text: {
    fontSize: 12,
  },
  image: {
    marginRight: 5,
    width: 100,
    height: 100,
    borderRadius: 20
  },
})

export default App;
