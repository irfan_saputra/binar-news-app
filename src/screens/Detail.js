import React from 'react';
import { View, Button, FlatList, StyleSheet, Image, TouchableOpacity } from 'react-native';
import {WebView} from 'react-native-webview';


class Detail extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          isLoading: true,
          dataSource: [],
          uri:''
        }
      }
    render() {
        //console.log(this.props.route.params.url)
        return(
             <WebView style={styles.container} 
                source={{uri:this.props.route.params.url}} /> 
        )
    }
}

const styles = StyleSheet.create({
    container:{
      flex:1,
      justifyContent:'center',
      alignItems:'center'
}})

export default Detail;