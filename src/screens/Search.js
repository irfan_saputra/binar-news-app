import React, { Component } from 'react';
import { Text, Image, View, FlatList, ActivityIndicator, StyleSheet, TextInput } from 'react-native';
import Config from "react-native-config";
import { TouchableOpacity } from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/FontAwesome';

const axios = require('axios');
//const moment = require('moment');


export default class SearchScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            newsData: [],
            page: 0,
            searchQuery: ''
        }
    }

    async loadNews(page) {
        let currPage = page + 1;

        axios.get(Config.API_URL + '/everything?q='+this.state.searchQuery+'&page='+currPage+'&apiKey=' + Config.API_KEY,{
            params: {
                pageSize: 20,
            }
        }).then((result) => {
            const newsData = result.data.articles;

            if (currPage == 1) {
                this.setState({
                    newsData: newsData,
                    isLoading: false,
                    page: currPage
                })
            } else {
                this.setState({
                    newsData: this.state.newsData.concat(newsData),
                    isLoading: false,
                    page: currPage
                })
            }
        }).catch((error) => {
            alert(error);
        })
    }

    newsOnPress(url) {
        this.props.navigation.navigate('Detail', { url });
    }

    searchNews() {
        const { searchQuery } = this.state;

        if (searchQuery) {
            this.loadNews(0);
        }
    }

    renderNews = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => this.newsOnPress(item.url)} style={styles.containerNews}>
                <View style={styles.containerHeader}>
                    <Image style={styles.image} source={{ uri: item.urlToImage }}/>
                    <Text style={styles.title}>{item.title}</Text>
                </View>
                <Text style={[styles.text, {fontWeight: 'bold'}]}>{item.author}</Text>
                <Text style={styles.text}>{item.description}</Text>
                <Text style={[styles.text, {fontWeight: 'bold'}]}>{item.publishedAt}</Text>
            </TouchableOpacity>
        );
    }

    render() {
        const { isLoading, newsData, searchQuery } = this.state;

        return (
            <View style={styles.container}>
                <View style={styles.searchBarContainer}>
                    <TextInput
                        style={styles.searchBar}
                        onChangeText={text => this.setState({ searchQuery: text })}
                        autoFocus={true}
                        keyboardType="web-search"
                        onSubmitEditing={() => this.searchNews()}
                        value={searchQuery}
                    />

                    <TouchableOpacity onPress={() => this.searchNews()} style={styles.searchButton}>
                        <Ionicons name="search" size={40} color={'white'} />
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={newsData}
                    renderItem={this.renderNews}
                    onEndReached={() => this.loadNews(this.state.page)}
                    keyExtractor={item => item.url}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerLoading: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    container: {
        flex: 1,
    },
    containerHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 5,
    },
    containerNews: {
        padding: 10,
        borderWidth: StyleSheet.hairlineWidth,
        borderRadius: 15,
        margin: 10
    },
    title: {
        fontSize: 16,
        flex: 1,
        color: 'blue',
        marginRight: 5
    },
    text: {
        fontSize: 12,
    },
    image: {
        marginRight: 5,
        width: 100,
        height: 100,
        borderRadius: 20
    },
    searchBarContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 5
    },
    searchBar: {
        flex: 1,
        height: 40,
        borderColor: 'blue',
        borderBottomWidth: 1,
        marginHorizontal: 10
    },
    searchButton: {
        borderRadius: 2,
        backgroundColor: 'blue',
        marginRight: 5
    }
})
